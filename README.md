# INI support for the Go language

Introduction
------------

Quick concept ini-file parser to parse desktop-files as none of the existing
Go ini-file parsers would either parse or write working desktop-files.
