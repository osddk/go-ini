// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package ini

import (
	"bufio"
	"os"
	"regexp"
)

type Ini map[string]map[string]string

var reSection *regexp.Regexp = regexp.MustCompile(`^\b*\[(.+)\]\b*$`)
var reKeyVal *regexp.Regexp = regexp.MustCompile(`^\b*([a-zA-Z-]+(?:\[[a-zA-Z_]+\])?)\b*=\b*(.*)$`)

func ParseFile(inifile string) (ini Ini, err error) {
	file, err := os.Open(inifile)
	if err != nil {
		return nil, err
	}

	i := make(Ini)
	var section string = ""
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		s := scanner.Text()
		if m := reSection.FindStringSubmatch(s); len(m) == 2 {
			section = m[1]
			if i[section] == nil {
				i[section] = make(map[string]string)
			}
		} else if m := reKeyVal.FindStringSubmatch(s); len(m) == 3 {
			i[section][m[1]] = m[2]
		}
	}
	return i, nil
}

func (ini Ini) Write(name string) error {
	file, err := os.Create(name)
	if err != nil {
		return err
	}

	for sname, s := range ini {
		file.WriteString("[" + sname + "]" + "\n")
		for k, v := range s {
			file.WriteString(k + "=" + v + "\n")
		}
	}
	return nil
}
